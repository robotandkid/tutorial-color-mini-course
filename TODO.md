# TODO

- [x] favicon
- [x] scrolling, a11y links
- [x] external links open new tab
- [x] support deep links
- [ ] ex02
  - [x] use same-size swatches from ex01 in text
  - [x] use same-size swatches from ex01 in Canvas
  - [x] use narrower Canvas
- [x] add ex03 - coming soon!
- [ ] mobile
- [ ] blockquote should use terminal font
- [ ] Add appendix
  - [ ] Add luma source code
  - [ ] Add RGB to HSL source code
  - [ ] Add resources

```markdown
## Resources

- <https://jaejohns.com/8-color-theory-painting-exercises-to-improve-your-paintings>
- <https://theartofeducation.edu/packs/exploring-color-theory-watercolor>
- <http://marilynfenn.com/continued/color-theory-exercises>
- <http://www.beginnersschool.com/2015/10/27/basic-color-theory>
- [Really good explanation of chromatic, value](http://www.finkeldeistudio.com/how-to-articles/oil-acrylic-painting/35-understanding-hue-value-and-chromatic-intensity)
```
