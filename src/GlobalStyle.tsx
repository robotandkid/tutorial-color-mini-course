import React from "react";
import styled, { createGlobalStyle, ThemeProvider } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    @font-face {
        font-family: 'terminal_grotesque';
        src: url('/static/fonts/terminal-grotesque-webfont.woff2') format('woff2');
    }

    @font-face {
        font-family: 'happy_times';
        src: url('/static/fonts/happy-times-NG_regular_master_web.woff2') format('woff2');
    }

    @font-face {
        font-family: 'happy_times';
        src: url('/static/fonts/happy-times-NG_bold_master_web.woff2') format('woff2');
        font-weight: bold;
    }

    @font-face {
        font-family: 'happy_times';
        src: url('/static/fonts/happy-times-NG_italic_master_web.woff2') format('woff2');
        font-style: italic;
    }

    @font-face {
        font-family: 'appSans';
        src: url('/static/fonts/lack-regular-webfont.woff2') format('woff2');
    }

    @font-face {
        font-family: 'appSans';
        src: url('/static/fonts/lack-italic-webfont.woff2') format('woff2');
        font-style: italic;
    }

    body {
        font-family: 'happy_times', serif;
        font-size: 62.5%;
    }
`;

export const NormalizedAnchor = styled.a`
  text-decoration: none;
  color: inherit;
  cursor: pointer;

  &:visited {
    color: inherit;
  }
`;

const grayTheme = {
  gray1: `hsl(207, 9%, 56%)`,
  gray2: `hsl(205, 7%, 46%)`,
  gray3: `hsl(206, 7%, 40%)`,
  gray4: `hsl(228, 2%, 34%)`,
  gray5: `hsl(228, 3%, 32%)`,
};

interface Theme {
  colorMainHeading: string;
  colorAnchor: string;
  colorScrollUpButton: string;
  colorScrollUpButtonHover: string;
  colorScrollUpButtonClick: string;
  fontTerminal: string;
  pageArticleWidth: string;
}

const theme: Theme = {
  colorMainHeading: grayTheme.gray1,
  colorAnchor: grayTheme.gray1,
  colorScrollUpButton: grayTheme.gray1,
  colorScrollUpButtonHover: grayTheme.gray3,
  colorScrollUpButtonClick: grayTheme.gray5,
  fontTerminal: `"terminal_grotesque", sans-serif`,
  pageArticleWidth: "35rem",
};

export interface AppThemeProps {
  theme: Theme;
}

export const AppThemeProvider = (props: { children?: React.ReactNode }) => {
  return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
};
