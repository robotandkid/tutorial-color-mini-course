import { fabric } from "fabric";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { RGB } from "../exercises/Color.types";

export const canvasDim = {
  width: 1000,
  height: 400,
} as const;

export const canvasSwatch = {
  width: 100,
  height: 75,
} as const;

export interface CanvasSwatch {
  id: string;
  left: number;
  top: number;
  width: number;
  height: number;
  color: number | RGB;
  colorCSS: string;
  valueCSS: string;
}

export interface CanvasState {
  objects: Array<{
    id?: string;
    rgb?: RGB;
    angle: number;
    backgroundColor: string;
    fill: string;
    height: number;
    width: number;
    left: number;
    top: number;
    opacity: number;
    originX: string;
    originY: string;
    type: "rect";
    visible: boolean;
  }>;
}

/**
 * Returns a ref that can be passed to DOM elements
 * and a reference to the associated fabric canvas.
 *
 * @param opts.width
 * @param opts.height
 */
export const useFabric = (opts: {
  width: number;
  height: number;
  init?: (canvas: fabric.Canvas) => void;
}): {
  ref: React.MutableRefObject<null>;
  canvas: fabric.Canvas | undefined;
  state: CanvasState;
  syncCanvasState: () => void;
} => {
  const { init } = opts;
  const ref = useRef(null);
  const initCalled = useRef(false);
  const [canvas, setCanvas] = useState<fabric.Canvas | undefined>();
  const [state, setState] = useState<CanvasState>({ objects: [] });

  useEffect(
    function create() {
      const fabricInst = new fabric.Canvas(ref.current!);

      fabricInst.setDimensions({ width: opts.width, height: opts.height });

      setCanvas(fabricInst);
    },
    [opts.height, opts.width]
  );

  useEffect(
    function initialize() {
      if (canvas && !initCalled.current) {
        init?.(canvas);
        initCalled.current = true;
      }
    },
    [canvas, init]
  );

  const syncCanvasState = useCallback(() => {
    setState({ objects: canvas?.toObject(["id", "rgb"]).objects });
  }, [canvas]);

  useEffect(
    function keepStateUpdated() {
      canvas?.on("mouse:up", syncCanvasState);

      return () => {
        canvas?.off("mouse:up", syncCanvasState);
      };
    },
    [canvas, syncCanvasState]
  );

  return { ref, canvas, state, syncCanvasState };
};
