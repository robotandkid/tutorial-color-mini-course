import { RGB, toValueCSS, Value255 } from "../exercises/Color.types";

function Max(x: number, y: number, z: number) {
  return Math.max(x, Math.max(y, z));
}
function Min(x: number, y: number, z: number) {
  return Math.min(x, Math.min(y, z));
}
/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * https://stackoverflow.com/a/9493060/555493
 *
 * @param   {number}  h       The hue
 * @param   {number}  s       The saturation
 * @param   {number}  l       The lightness
 * @return  {Array}           The RGB representation
 */
export function hslToRgb({
  h,
  s,
  l,
}: {
  h: number;
  s: number;
  l: number;
}): RGB {
  var r, g, b;
  if (s === 0) {
    r = g = b = l; // achromatic
  } else {
    var hue2rgb = function hue2rgb(p: number, q: number, t: number) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1 / 6) return p + (q - p) * 6 * t;
      if (t < 1 / 2) return q;
      if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
      return p;
    };
    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1 / 3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1 / 3);
  }
  return {
    r: Math.round(r * 255),
    g: Math.round(g * 255),
    b: Math.round(b * 255),
  };
}

/**
 * https://tannerhelland.com/2011/10/01/grayscale-image-algorithm-vb6.html
 * https://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
 * @param {number} opts.r - [0, 255]
 * @param {number} opts.g - [0, 255]
 * @param {number} opts.b - [0, 255]
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function meanCSS({ r, g, b }: RGB) {
  const l = Math.floor((r + g + b) / 3.0);
  return toValueCSS(Value255(l));
}

/**
 * https://tannerhelland.com/2011/10/01/grayscale-image-algorithm-vb6.html
 * https://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
 * @param {number} opts.r - [0, 255]
 * @param {number} opts.g - [0, 255]
 * @param {number} opts.b - [0, 255]
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function desaturateCSS({ r, g, b }: RGB) {
  const l = Math.floor((Max(r, g, b) + Min(r, g, b)) / 2.0);
  return toValueCSS(Value255(l));
}

/**
 * https://tannerhelland.com/2011/10/01/grayscale-image-algorithm-vb6.html
 * https://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
 * @param {number} opts.r - [0, 255]
 * @param {number} opts.g - [0, 255]
 * @param {number} opts.b - [0, 255]
 */
export function lumaCSS(opts: RGB) {
  return toValueCSS(luma(opts));
}

/**
 * https://tannerhelland.com/2011/10/01/grayscale-image-algorithm-vb6.html
 * https://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
 *
 * @param {number} opts.r - [0, 255]
 * @param {number} opts.g - [0, 255]
 * @param {number} opts.b - [0, 255]
 * @returns {number} [0, 255]
 */
export function luma({ r, g, b }: RGB) {
  return Value255(r * 0.2126 + g * 0.7152 + b * 0.0722);
}
