/**
 * Return an integer between [0, max-1] inclusive
 *
 * @param max number
 */
export const randomNatural = (max: number) => Math.floor(Math.random() * max);

export const randomHue = () => randomNatural(360);

export const normalizedRandomHue = () => Math.random();

export const randomRGB = () => randomNatural(256);
