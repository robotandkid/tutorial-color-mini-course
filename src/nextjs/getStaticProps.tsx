import React from "react";
import {
  TableOfContentsItem,
  TableOfContentsProvider,
} from "../components/TableOfContents";
import { renderToString } from "react-dom/server";
import { AppArticles } from "../App";

export const getStaticProps = async () => {
  let initialTableOfContents: TableOfContentsItem[] = [];
  const getItems = (items: TableOfContentsItem[]) => {
    initialTableOfContents = [...items];
  };

  const app = () => (
    <TableOfContentsProvider setItemsForSSR={getItems}>
      <AppArticles />
    </TableOfContentsProvider>
  );

  renderToString(app());

  return {
    props: {
      initialTableOfContents,
    },
  };
};
