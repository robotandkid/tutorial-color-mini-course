import React from "react";
import { Page } from "./components/components";
import { ScrollUpButton } from "./components/ScrollUpButton";
import {
  TableOfContents,
  TableOfContentsItem,
  TableOfContentsProvider,
} from "./components/TableOfContents";
import { Definitions } from "./exercises/definitions";
import { Exercise01 } from "./exercises/ex01";
import { Exercise02 } from "./exercises/ex02";
import { Intro } from "./exercises/intro";
import { Exercise03 } from "./exercises/ex03";

export function AppArticles() {
  return (
    <>
      <Intro />
      <Definitions />
      <Exercise01 />
      <Exercise02 />
      <Exercise03 />
    </>
  );
}

function App(props: { initialTableOfContents?: TableOfContentsItem[] }) {
  return (
    <Page>
      <div className="App">
        <TableOfContentsProvider initialItems={props.initialTableOfContents}>
          <TableOfContents />
          <AppArticles />
        </TableOfContentsProvider>
      </div>
      <ScrollUpButton />
    </Page>
  );
}

export default App;
