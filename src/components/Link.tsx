import React, {
  DetailedHTMLProps,
  AnchorHTMLAttributes,
  ReactNode,
  useEffect,
  useRef,
  useCallback,
} from "react";
import styled from "styled-components";
import { AppThemeProps, NormalizedAnchor } from "../GlobalStyle";
import NextLink from "next/link";
import { useRouter } from "next/router";

const StyledExternalLink = styled(NormalizedAnchor)`
  font-style: italic;
  color: ${(props: AppThemeProps) => props.theme.colorAnchor};
  border-bottom: 1px dashed ${(props: AppThemeProps) => props.theme.colorAnchor};

  &:visited {
    color: ${(props: AppThemeProps) => props.theme.colorAnchor};
  }
`;

export const ExternalLink = ({
  ref,
  target,
  children,
  ...props
}: DetailedHTMLProps<
  AnchorHTMLAttributes<HTMLAnchorElement>,
  HTMLAnchorElement
>) => (
  <StyledExternalLink tabIndex={0} {...props} target="_blank">
    {children}
  </StyledExternalLink>
);

const StyledInternalLink = styled(NormalizedAnchor)`
  font-style: italic;
  color: ${(props: AppThemeProps) => props.theme.colorAnchor};
  border-bottom: 1px dashed ${(props: AppThemeProps) => props.theme.colorAnchor};

  &:visited {
    color: ${(props: AppThemeProps) => props.theme.colorAnchor};
  }
`;

const _target = (href: string | undefined) =>
  href?.charAt(0) === "#" ? href?.substring(1) : undefined;

export const InternalLink = ({
  ref,
  href,
  children,
  ...props
}: DetailedHTMLProps<
  AnchorHTMLAttributes<HTMLAnchorElement>,
  HTMLAnchorElement
>) => {
  const target = _target(href);
  const { push } = useRouter() || {};

  const onKeyUp = useCallback(
    (e: React.KeyboardEvent<HTMLAnchorElement>) => {
      if (e.key === " " || e.key === "Enter") {
        push({ pathname: "/", query: { link: target } }, undefined, {
          shallow: true,
        });
      }
    },
    [push, target]
  );

  return (
    <NextLink href={`/?link=${target}`} shallow>
      <StyledInternalLink tabIndex={0} {...props} onKeyUp={onKeyUp}>
        {children}
      </StyledInternalLink>
    </NextLink>
  );
};

export const StyledLinkTarget = styled.div``;

export const LinkTarget = (props: {
  id: string;
  wrapper: (props: {
    ref: any;
    id: string;
    tabIndex: number;
    children?: React.ReactNode;
  }) => JSX.Element;
  children?: React.ReactNode;
}) => {
  const { query = {} } = useRouter() || {};
  const ref = useRef<HTMLDivElement | null>();
  const { wrapper: Wrapper } = props;

  useEffect(() => {
    let unsubscribe: number | undefined;

    if (query.link === props.id) {
      window.scrollTo({
        top: ref.current?.getBoundingClientRect().top,
        left: 0,
        behavior: "smooth",
      });

      unsubscribe = setTimeout(() => {
        ref.current?.focus();
      }, 1000);

      return () => {
        unsubscribe && clearTimeout(unsubscribe);
      };
    }
  }, [props.id, query.link]);

  return (
    <Wrapper ref={ref as any} id={props.id} tabIndex={0}>
      {props.children}
    </Wrapper>
  );
};
