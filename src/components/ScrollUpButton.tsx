import React, { useEffect, useRef, useState, useCallback } from "react";
import ReactDOM from "react-dom";
import styled, { createGlobalStyle } from "styled-components";
import { AppThemeProps } from "../GlobalStyle";
import { useRouter } from "next/router";

export const scrollUpButtonId = "scroll-up-button";

function useListenToScroll() {
  const scroll = useRef({ lastKnownScrollY: 0, ticking: false });
  const [showScrollUpButton, setShowScrollUpButton] = useState(false);

  useEffect(() => {
    function trackScrollY() {
      scroll.current.lastKnownScrollY = window.scrollY;
      requestTick();
    }

    function requestTick() {
      if (!scroll.current.ticking) {
        requestAnimationFrame(update);
      }
      scroll.current.ticking = true;
    }

    function update() {
      scroll.current.ticking = false;

      if (
        !showScrollUpButton &&
        scroll.current.lastKnownScrollY >= window.innerHeight
      ) {
        setShowScrollUpButton(true);
      } else if (
        showScrollUpButton &&
        scroll.current.lastKnownScrollY < window.innerHeight
      ) {
        setShowScrollUpButton(false);
      }
    }

    window.addEventListener("scroll", trackScrollY);

    return () => window.removeEventListener("scroll", trackScrollY);
  }, [showScrollUpButton]);

  return showScrollUpButton;
}

const GlobalStyle = createGlobalStyle`
`;

const ButtonContainer = styled.div`
  position: fixed;
  right: 1.75rem;
  top: calc(100vh - 10rem);
  font-weight: bold;
  font-family: ${(props: AppThemeProps) => props.theme.fontTerminal};
  border: none;
  border-radius: 50%;
  background-color: ${(props: AppThemeProps) =>
    props.theme.colorScrollUpButton};
  color: white;
  width: 4rem;
  height: 4rem;
  padding: 0.4rem;

  & > div {
    transform: rotateZ(90deg);
    font-size: 4rem;
    line-height: 1;
    margin-top: 1rem;
  }

  &:hover {
    background-color: ${(props: AppThemeProps) =>
      props.theme.colorScrollUpButtonHover};
  }

  &:active {
    background-color: ${(props: AppThemeProps) =>
      props.theme.colorScrollUpButtonClick};
    transform: scale(1.1);
  }

  animation: grow 0.25s;

  @keyframes grow {
    from {
      transform: scale(0);
    }

    to {
      transform: scale(1);
    }
  }
`;

const Button = (props: { children?: React.ReactNode; onClick: () => void }) => {
  return (
    <ButtonContainer onClick={props.onClick} onKeyDown={props.onClick}>
      <div
        data-ref="#"
        tabIndex={0}
        role="link"
        aria-label="go back to beginning"
      >
        {props.children}
      </div>
    </ButtonContainer>
  );
};

function ScrollUpButtonPortal() {
  const show = useListenToScroll();
  const el = useRef<HTMLDivElement | undefined>();
  const { push } = useRouter() || {};

  useEffect(() => {
    const modalRoot = document.getElementById(scrollUpButtonId);

    el.current = document.createElement("div");
    modalRoot?.appendChild(el.current);

    return () => {
      modalRoot?.removeChild(el.current!);
    };
  }, []);

  const scrollToTop = useCallback(() => {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    push({ pathname: "/", query: {} }, undefined, { shallow: true });
  }, [push]);

  return show && el.current
    ? ReactDOM.createPortal(
        <Button onClick={scrollToTop}>&#x00AB;</Button>,
        el.current
      )
    : null;
}

export function ScrollUpButton() {
  return (
    <>
      <GlobalStyle />
      <ScrollUpButtonPortal />
    </>
  );
}
