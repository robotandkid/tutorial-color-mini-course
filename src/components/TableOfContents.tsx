import React, { useContext, useMemo, useRef, useState } from "react";
import styled from "styled-components";
import { AppThemeProps } from "../GlobalStyle";
import { InternalLink } from "./Link";

export interface TableOfContentsItem {
  id: string;
  index: number;
  heading: string;
}

interface TableOfContentsState {
  items: ReadonlyArray<TableOfContentsItem>;
}

interface TableOfContentsSetter {
  setItem(item: TableOfContentsItem): void;
}

const TableOfContentsStateContext = React.createContext<TableOfContentsState>({
  items: [],
});

const TableOfContentsSetterContext = React.createContext<TableOfContentsSetter>(
  {
    setItem() {},
  }
);

export const TableOfContentsProvider = (props: {
  initialItems?: TableOfContentsItem[];
  setItemsForSSR?: (items: TableOfContentsItem[]) => void;
  children?: React.ReactNode;
}) => {
  const items = useRef(props.initialItems || []);
  const [count, setCount] = useState(0);

  const { setItemsForSSR } = props;

  const setterValue = useMemo(
    () => ({
      setItem(item: TableOfContentsItem) {
        if (!items.current.find((x) => x.id === item.id)) {
          items.current.push(item);
          items.current.sort((a, b) => a.index - b.index);
          setCount((count) => count + 1);
          setItemsForSSR?.(items.current);
        }
      },
    }),
    [setItemsForSSR]
  );

  const stateValue = useMemo(() => ({ items: items.current, count }), [count]);

  return (
    <TableOfContentsSetterContext.Provider value={setterValue}>
      <TableOfContentsStateContext.Provider value={stateValue}>
        {props.children}
      </TableOfContentsStateContext.Provider>
    </TableOfContentsSetterContext.Provider>
  );
};

interface TableOfContentsSetterWorkerProps {
  item: TableOfContentsItem;
  setItem: (item: TableOfContentsItem) => void;
}

export class TableOfContentsSetterWorker extends React.Component<
  TableOfContentsSetterWorkerProps,
  {}
> {
  constructor(props: TableOfContentsSetterWorkerProps) {
    super(props);

    props.setItem(props.item);
  }

  render() {
    return null;
  }
}

/**
 * Usage: use this as a child component when the parent needs to set the TOC.
 *
 * Exists so that a component can set the TOC without triggering
 * an unnecessary render on itself.
 */
export function TableOfContentsSetter(props: { item: TableOfContentsItem }) {
  const { setItem } = useContext(TableOfContentsSetterContext);

  return <TableOfContentsSetterWorker item={props.item} setItem={setItem} />;
}

const Item = styled(InternalLink)`
  display: block;
  font-family: ${(props: AppThemeProps) => props.theme.fontTerminal};
  font-size: 2rem;
  font-style: normal !important;
  color: ${(props: AppThemeProps) => props.theme.colorMainHeading};
  margin-bottom: 1.25rem;

  :last-child {
    margin-bottom: 0 !important;
  }

  &:visited {
    color: ${(props: AppThemeProps) => props.theme.colorMainHeading};
  }

  & > span {
    border-bottom: 1px dashed
      ${(props: AppThemeProps) => props.theme.colorMainHeading};
  }
`;

export const TableOfContents = () => {
  const { items } = useContext(TableOfContentsStateContext);

  return (
    <nav>
      {items.map(({ id, heading }) => (
        <Item key={id} href={`#${id}`}>
          <span>{heading}</span>
        </Item>
      ))}
    </nav>
  );
};
