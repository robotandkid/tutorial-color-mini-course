import React, { useCallback, useRef, useEffect } from "react";
import styled from "styled-components";
import { AppThemeProps } from "../GlobalStyle";
import { useRouter } from "next/router";
import { LinkTarget, StyledLinkTarget } from "./Link";

export const Page = styled.main`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

export const Article = styled.article`
  max-width: ${(props: AppThemeProps) => props.theme.pageArticleWidth};
`;

const ArticleHeadingContainer = styled.h1`
  font-family: ${(props: AppThemeProps) => props.theme.fontTerminal};
  font-size: 4rem;
  color: ${(props: AppThemeProps) => props.theme.colorMainHeading};
  text-transform: uppercase;
  width: 100vw;
  position: relative;
  left: calc(-50vw + 50%);
  display: flex;
  justify-content: center;

  & > div {
    min-width: ${(props: AppThemeProps) => props.theme.pageArticleWidth};
  }
`;

const customDiv = styled.div``;

export const ArticleHeading = ({
  ref,
  as,
  id,
  children,
  ...props
}: Omit<React.HTMLProps<HTMLHeadingElement>, "id"> & { id: string }) => (
  <ArticleHeadingContainer {...props}>
    <LinkTarget wrapper={customDiv} id={id}>
      {children}
    </LinkTarget>
  </ArticleHeadingContainer>
);

export const ArticleContent = styled.div`
  display: block;
  font-size: 1.75rem;
`;

export const ArticleList = styled.ul`
  & > li {
    margin-top: 0.7rem;
    margin-bottom: 0.7rem;
  }
`;

export const ArticleButton = styled.button`
  font-size: 1.5rem;
  font-family: "happy_times";
  font-weight: bold;
  text-transform: uppercase;
  padding: 0.75rem;
`;

const RadioInput = styled.input.attrs(() => ({
  type: "radio",
}))``;

const ArticleToggleContainer = styled.div`
  display: flex;
  align-items: baseline;
  font-size: 1.5rem;
  font-family: "happy_times";
  font-weight: bold;
  text-transform: uppercase;
  margin-top: 1rem;
  margin-bottom: 1rem;

  & > span {
    margin-right: 0.75rem;
  }

  & > input {
    height: 1rem;
  }

  & > label {
    margin-left: 0.25rem;
    margin-right: 0.5rem;
  }

  & > label:last-child {
    margin-right: 0;
  }
`;

export const ArticleToggleLabel = (props: { children?: string }) => (
  <>{props.children}</>
);

export const ArticleToggle = (props: {
  id: string;
  on: boolean;
  onToggle: (value: boolean) => void;
  children?: [
    React.ReactElement<{ children?: string }, string>,
    React.ReactElement<{ children?: string }, string>,
    React.ReactElement<{ children?: string }, string>
  ];
}) => {
  const description = props.children?.[0]!.props.children;
  const onLabel = props.children?.[1]!.props.children;
  const offLabel = props.children?.[2]!.props.children;
  const descriptionId = `${props.id}-description`;
  const onId = `${props.id}-on`;
  const offId = `${props.id}-off`;
  const { onToggle } = props;

  const toggle = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      if (e.currentTarget.name === onLabel) {
        onToggle(true);
      } else {
        onToggle(false);
      }
    },
    [onLabel, onToggle]
  );

  return (
    <ArticleToggleContainer>
      <span id={descriptionId} aria-hidden>
        {description}
      </span>
      <RadioInput
        id={onId}
        name={onLabel}
        checked={props.on}
        onChange={toggle}
        aria-describedby={descriptionId}
      />
      <label htmlFor={onId}>{onLabel}</label>
      <RadioInput
        id={offId}
        name={offLabel}
        checked={!props.on}
        onChange={toggle}
        aria-describedby={descriptionId}
      />
      <label htmlFor={offId}>{offLabel}</label>
    </ArticleToggleContainer>
  );
};

export const Blockquote = styled.blockquote`
  border: 1px solid black;
  border-radius: 5px;
  padding: 0.75em 0.75em;
  margin: 0.25em;
  font-size: 0.8em;
  font-family: appSans, sans-serif;
  line-height: 1.6;

  & b {
    font-style: italic;
  }
`;

export const CanvasContainer = styled.div`
  width: 100vw;
  position: relative;
  left: calc(-50vw + 50%);
  display: flex;
  justify-content: center;

  & > div.canvas-container {
    box-shadow: 5px 5px 11px 0px #ccc;
    border: 1px solid #ccc;
    margin-top: 1rem;
  }
`;
