import styled, { css } from "styled-components";
import React, { useEffect, useState } from "react";

export type SwatchProps = {
  color?: string;
  minWidth?: string;
  minHeight?: string;
  padding?: string;
};

const BaseSwatch = styled.div<SwatchProps>`
  display: inline-flexbox;
  min-width: ${(props: SwatchProps) => props.minWidth || "2.5em"};
  min-height: ${(props: SwatchProps) => props.minHeight || "1.25em"};
  padding: ${(props: SwatchProps) => props.padding || "0.25em"};
  vertical-align: text-bottom;
  justify-content: center;

  ${(props: SwatchProps) =>
    props.color &&
    css`
      background: ${props.color};
    `}
`;

const defaultColor = "white";

export const Swatch = ({
  color,
  children,
  ...props
}: SwatchProps & React.HTMLAttributes<HTMLDivElement>) => {
  const [hasRendered, setHasRendered] = useState(false);

  // This complexity is needed because of NextJS:
  // - on export, the rendered color needs to match the
  //   the color on initial render
  // - so we render initially as white,
  //   then apply the desired color on mount.
  useEffect(() => {
    if (!hasRendered) {
      setHasRendered(true);
    }
  }, [hasRendered]);

  const actualColor = hasRendered && color ? color : defaultColor;

  return (
    <BaseSwatch color={actualColor} {...props}>
      {children}
    </BaseSwatch>
  );
};

export const ColorStrip = styled.div`
  margin-top: 0.4em;

  & + & {
    margin-top: 0;
  }
`;
