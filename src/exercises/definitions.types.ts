export const definitionIds = [
  "def-hue",
  "def-color",
  "def-tint",
  "def-tone",
  "def-shade",
  "def-value",
  "def-saturation",
  "def-chromatic",
  "def-achromatic",
  "def-fue",
  "def-color",
  "def-tint",
  "def-tone",
  "def-shade",
  "def-value",
  "def-saturation",
  "def-chromatic",
  "def-achromatic",
  "def-boundary-line"
] as const;

export type DefinitionIds = typeof definitionIds[number];
