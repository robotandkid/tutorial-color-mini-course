import { fabric } from "fabric";
import React, { useCallback, useMemo, useRef, useState } from "react";
import {
  Article,
  ArticleButton,
  ArticleContent,
  ArticleHeading,
  ArticleList,
  ArticleToggle,
  ArticleToggleLabel,
  CanvasContainer,
} from "../components/components";
import { TableOfContentsSetter } from "../components/TableOfContents";
import {
  canvasDim,
  CanvasState,
  CanvasSwatch,
  canvasSwatch,
  useFabric,
} from "../fabric/Fabric";
import { luma, lumaCSS } from "../utils/grayscale";
import { getId } from "../utils/id";
import { randomNatural, randomRGB } from "../utils/random";
import { diff, RGB, toRgbCSS, toValueCSS, Value255 } from "./Color.types";
import { Def, DefLink } from "./definitions";
import { ColorStrip, Swatch } from "./Swatch";

const rectangleWidth = 6;

const RandomStrip = (props: { colors: string[] }) => {
  return (
    <ColorStrip>
      {props.colors.map((rgb, i) => (
        <Swatch key={i} color={rgb}></Swatch>
      ))}
    </ColorStrip>
  );
};

/**
 * @param props.start - is the starting palette
 * @param props.palette - is the palette we'll be using to match
 * @param props.paletteStart - is the randomly chosen color to start
 */
const solve = (props: {
  start: RGB[];
  palette: RGB[];
  paletteStart: number;
}) => {
  const baseValues = props.start.map(luma);
  const paletteValues = props.palette.map(luma);
  const startingBaseValue = baseValues[0];
  const startingPaletteValue = paletteValues[props.paletteStart];

  const targetDifference = Math.abs(
    diff(startingBaseValue, startingPaletteValue)
  );
  const usedPaletteMap = { [props.paletteStart]: true };

  const findClosestPaletteValueIndex = (value: Value255) => {
    let smallestDiff = Number.MAX_SAFE_INTEGER;
    let match: number | undefined;

    for (let j = 0; j < paletteValues.length; ++j) {
      if (usedPaletteMap[j]) {
        continue;
      }

      const valueDiff = Math.abs(diff(value, paletteValues[j]));

      if (Math.abs(valueDiff - targetDifference) <= smallestDiff) {
        smallestDiff = Math.abs(valueDiff - targetDifference);
        match = j;
      }
    }

    return match;
  };

  const solution: RGB[] = [props.palette[props.paletteStart]];

  for (let i = 1; i < props.start.length; ++i) {
    const match = findClosestPaletteValueIndex(baseValues[i]);

    if (typeof match !== "undefined") {
      usedPaletteMap[match] = true;
      solution.push(props.palette[match]);
    }
  }

  return solution;
};

/**
 * Ensures colors generated don't have similar values.
 *
 * @param count
 */
const generateRGBwithVaryingValues = (
  count: number
): Array<{ rgb: RGB; value: Value255 }> => {
  const colors: Array<{ rgb: RGB; value: Value255 }> = [];
  // Divide tone [0, 255] into bins
  // with binCount = count and binWidth = 255/count;
  const binCount = count;
  const binWidth = Math.floor(255 / binCount);
  const colorBins: { [key in string]: true } = {};
  const binNumber = (value: number) => Math.floor(value / binWidth);

  while (colors.length < count) {
    const rgb = {
      r: randomRGB(),
      g: randomRGB(),
      b: randomRGB(),
    };
    const value = luma(rgb);
    const bin = binNumber(value.number255);

    if (!colorBins[bin]) {
      colorBins[bin] = true;
      colors.push({ rgb, value });
    }
  }

  return colors;
};

const heading = "EX02: Boundaries";
const id = "ex02";

const Exercise = () => {
  const {
    startColorsCSS,
    paletteColorsCSS,
    randomStart,
    solutionCSS,
    startValuesCSS,
    solutionValuesCSS,
    // memoize to keep from accidentally triggering new swatches
    // (which is annoying)
  } = useMemo(() => {
    const rgbStartColors = generateRGBwithVaryingValues(rectangleWidth).map(
      (x) => x.rgb
    );
    const rgbPaletteColors = generateRGBwithVaryingValues(rectangleWidth).map(
      (x) => x.rgb
    );
    const startColorsCSS = rgbStartColors.map(toRgbCSS);
    const paletteColorsCSS = rgbPaletteColors.map(toRgbCSS);
    const randomStart = randomNatural(paletteColorsCSS.length);
    const solution = solve({
      start: rgbStartColors,
      palette: rgbPaletteColors,
      paletteStart: randomStart,
    });
    const solutionValues = solution.map(luma);

    return {
      rgbStartColors,
      startColorsCSS,
      paletteColorsCSS,
      randomStart,
      solution,
      solutionCSS: solution.map(toRgbCSS),
      startValuesCSS: rgbStartColors.map(lumaCSS),
      solutionValuesCSS: solutionValues.map(toValueCSS),
    };
  }, []);

  return (
    <ArticleContent>
      <ArticleHeading id={id}>{heading}</ArticleHeading>
      <p>
        Two colors create a line where they meet. This line is called the{" "}
        <Def id="def-boundary-line">boundary line</Def>. The line is either{" "}
        <i>soft</i> or <i>hard</i> according to{" "}
        <DefLink href="def-value">values</DefLink>. If the values are close, the
        boundary line will be <i>soft</i>; and if one value is dark and the
        other light, the boundary line will be <i>hard</i>.
      </p>
      <ArticleList>
        <li>
          Generate 6 different hues with varying values. Place the hues randomly
          next to each other horizontally, forming a rectangle.
          <RandomStrip colors={startColorsCSS} />
        </li>
        <li>
          Generate 6 more hues.
          <RandomStrip colors={paletteColorsCSS} />
        </li>
        <li>
          Choose one hue at random and place it directly under the left-most
          hue. This creates a boundary line with the rectangle and the chosen
          hue.
          <RandomStrip colors={startColorsCSS} />
          <ColorStrip>
            <Swatch color={paletteColorsCSS[randomStart]} />
          </ColorStrip>
        </li>
        <li>
          Using the remaining hues, try to match the softness or hardness of the
          boundary line formed in the previous step on the succeeding 5
          divisions.
        </li>
        <li>
          The final result is a rectangle horizontally divided in the middle,
          and having 6 vertical divisions composed of 12 color swatches. The
          horizontal center line should have the same degree of hardness or
          softness from left to right regardless of what hues or values are
          chosen:
          <RandomStrip colors={startColorsCSS} />
          <RandomStrip colors={solutionCSS} />
        </li>
        <li>
          Here is the solution as values:
          <RandomStrip colors={startValuesCSS} />
          <RandomStrip colors={solutionValuesCSS} />
        </li>
      </ArticleList>
    </ArticleContent>
  );
};

function randomColors(opts: {
  top: number;
  left: number;
  swatchCount: number;
  swatchWidth: number;
  swatchHeight: number;
}): CanvasSwatch[] {
  return generateRGBwithVaryingValues(opts.swatchCount).map(
    ({ rgb, value }, i) => {
      return {
        id: getId(),
        top: opts.top,
        left: opts.left + i * opts.swatchWidth,
        color: rgb,
        colorCSS: toRgbCSS(rgb),
        valueCSS: toValueCSS(value),
        width: opts.swatchWidth,
        height: opts.swatchHeight,
      };
    }
  );
}

function addSwatches(
  canvas: fabric.Canvas,
  swatches: CanvasSwatch[],
  selectable: boolean
) {
  swatches.forEach((swatch) => {
    const rect = new fabric.Rect({
      top: swatch.top,
      left: swatch.left,
      width: swatch.width,
      height: swatch.height,
      fill: swatch.colorCSS,
    });
    rect.set("id" as any, swatch.id);
    rect.set("rgb" as any, swatch.color);
    rect.set("selectable", selectable);
    canvas.add(rect);
  });
}

function _generateSwatches(canvas: fabric.Canvas | undefined) {
  if (!canvas) {
    return;
  }

  canvas.remove(...canvas.getObjects());

  const start = randomColors({
    top: 50,
    left: 200,
    swatchCount: rectangleWidth,
    swatchWidth: canvasSwatch.width,
    swatchHeight: canvasSwatch.height,
  });

  const palette = randomColors({
    top: 250,
    left: 200,
    swatchCount: rectangleWidth,
    swatchWidth: canvasSwatch.width,
    swatchHeight: canvasSwatch.height,
  });

  addSwatches(canvas, start, false);
  addSwatches(canvas, palette, true);
}

function useToggleValues(
  canvas: fabric.Canvas | undefined,
  state: CanvasState,
  syncCanvasState: () => void
): [boolean, () => void] {
  const prevObjects = useRef<CanvasState["objects"]>(state.objects);
  const [on, setOn] = useState(false);

  const toggleValues = useCallback(() => {
    if (!on) {
      setOn(() => {
        prevObjects.current = [...state.objects];
        canvas?.forEachObject((obj) => {
          const rgb = obj.get("rgb" as any) as RGB | undefined;

          rgb && obj.setColor(lumaCSS(rgb));
        });
        canvas?.renderAll();
        syncCanvasState();
        return true;
      });
    } else {
      setOn(() => {
        canvas?.forEachObject((obj) => {
          const id = obj.get("id" as any) as string | undefined;
          const prevObj = prevObjects.current.find((x) => x.id === id);

          prevObj && obj.setColor(prevObj.fill);
        });
        canvas?.renderAll();
        syncCanvasState();
        return false;
      });
    }
  }, [canvas, on, state.objects, syncCanvasState]);

  return [on, toggleValues];
}

function useCanvas() {
  const { ref, canvas, state, syncCanvasState } = useFabric({
    width: canvasDim.width,
    height: canvasDim.height,
    init() {
      generateSwatches();
    },
  });

  const generateSwatches = useCallback(() => {
    _generateSwatches(canvas);
    syncCanvasState();
  }, [canvas, syncCanvasState]);

  const [valuesOn, toggleValues] = useToggleValues(
    canvas,
    state,
    syncCanvasState
  );

  return {
    generateSwatches,
    valuesOn,
    toggleValues,
    canvasRef: ref,
  };
}

const Canvas = () => {
  const { generateSwatches, canvasRef, valuesOn, toggleValues } = useCanvas();

  return (
    <>
      <ArticleButton onClick={generateSwatches}>
        Create new swatches
      </ArticleButton>
      <ArticleToggle id="ex02" on={valuesOn} onToggle={toggleValues}>
        <ArticleToggleLabel>Toggle Values</ArticleToggleLabel>
        <ArticleToggleLabel>On</ArticleToggleLabel>
        <ArticleToggleLabel>Off</ArticleToggleLabel>
      </ArticleToggle>
      <CanvasContainer>
        <canvas ref={canvasRef}></canvas>,
      </CanvasContainer>
    </>
  );
};

export const Exercise02 = () => {
  return (
    <>
      <TableOfContentsSetter item={{ index: 3, heading, id }} />
      <Article>
        <section>
          <Exercise />
        </section>
        <section style={{ marginTop: "3rem" }}>
          <Canvas />
        </section>
      </Article>
    </>
  );
};
