import { fabric } from "fabric";
import React, { useCallback } from "react";
import {
  Article,
  ArticleButton,
  ArticleContent,
  ArticleHeading,
  ArticleList,
  CanvasContainer,
} from "../components/components";
import { TableOfContentsSetter } from "../components/TableOfContents";
import { CanvasSwatch, useFabric, canvasSwatch } from "../fabric/Fabric";
import { hslToRgb, lumaCSS } from "../utils/grayscale";
import { getId } from "../utils/id";
import { normalizedRandomHue } from "../utils/random";
import { RGB } from "./Color.types";
import { DefLink } from "./definitions";

const swatchCount = 12;

const Exercise = () => (
  <ArticleContent>
    <p>
      Every hue has a corresponding black or gray{" "}
      <DefLink href="def-value">value</DefLink>. In the canvas below, each hue
      is shown with its value underneath.
    </p>
    <ArticleList>
      <li>
        Arrange the values horizontally into a progression with the darkest
        value on the left. The <i>swatches</i> should touch one another.
      </li>
      <li>
        The result should be a horizontal rectangle divided in half vertically
        and divided horizontally into {swatchCount} segments with hues on the
        top and their equivalent values on the bottom.
      </li>
    </ArticleList>
  </ArticleContent>
);

// create the swatches

const randomColorScheme = () => {
  const normalizedHue = normalizedRandomHue();
  const hue = Math.floor(normalizedHue * 360);
  const rgb = hslToRgb({
    h: normalizedHue,
    s: 1.0,
    l: 0.5,
  });

  return {
    hue,
    normalizedHue,
    hueCSS: `hsl(${hue}, 100%, 50%)`,
    valueCSS: lumaCSS(rgb),
  };
};

type CalculateGrayFillFn = (rgb: RGB) => string;

function createSwatches(opts: {
  top: number;
  left: number;
  swatchSpacing: number;
  swatchCount: number;
  swatchWidth: number;
  swatchHeight: number;
  calculateGrayFn: CalculateGrayFillFn;
}): Array<CanvasSwatch> {
  const swatches: Array<CanvasSwatch> = [];

  for (let i = 0; i < opts.swatchCount; ++i) {
    const { normalizedHue, hueCSS, valueCSS } = randomColorScheme();
    swatches.push({
      id: getId(),
      top: opts.top,
      left: opts.left + i * (opts.swatchWidth + opts.swatchSpacing),
      color: normalizedHue,
      colorCSS: hueCSS,
      valueCSS: valueCSS,
      width: opts.swatchWidth,
      height: opts.swatchHeight,
    });
  }

  return swatches;
}

function addSwatches(canvas: fabric.Canvas, swatches: Array<CanvasSwatch>) {
  swatches.forEach((swatch) => {
    const rect = new fabric.Rect({
      fill: swatch.colorCSS,
      width: swatch.width,
      height: swatch.height,
    });
    const shade = new fabric.Rect({
      fill: swatch.valueCSS,
      width: swatch.width,
      height: swatch.height,
      top: swatch.height,
    });
    const group = new fabric.Group([rect, shade], {
      left: swatch.left,
      top: swatch.top,
    });
    group.set("selectable", true);
    canvas.add(group);
  });
}

function useCanvas() {
  const { ref, canvas } = useFabric({
    width: 1000,
    height: 400,
    init(canvas) {
      canvas.selection = true;

      generateSwatches();
    },
  });

  const generateSwatches = useCallback(() => {
    if (!canvas) {
      return;
    }

    canvas.remove(...canvas.getObjects());

    const swatches = createSwatches({
      top: 50,
      left: 10,
      swatchSpacing: 10,
      swatchCount,
      swatchWidth: canvasSwatch.width,
      swatchHeight: canvasSwatch.height,
      calculateGrayFn: lumaCSS,
    });

    addSwatches(canvas, swatches);
  }, [canvas]);

  return {
    generateSwatches,
    canvasRef: ref,
  };
}

const heading = "EX01: Hue as Value";
const id = "ex01";

export const Exercise01 = () => {
  const { generateSwatches, canvasRef } = useCanvas();

  return (
    <>
      <TableOfContentsSetter item={{ index: 2, heading, id }} />
      <Article>
        <ArticleHeading id={id}>EX01: Hue as Value</ArticleHeading>
        <section>
          <Exercise />
          <ArticleButton onClick={generateSwatches}>
            Create new swatches
          </ArticleButton>
        </section>
        <section style={{ marginTop: "1rem" }}>
          <CanvasContainer>
            <canvas ref={canvasRef}></canvas>,
          </CanvasContainer>
        </section>
      </Article>
    </>
  );
};
