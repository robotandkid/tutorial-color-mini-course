import React from "react";
import {
  Article,
  ArticleContent,
  ArticleHeading,
} from "../components/components";
import { TableOfContentsSetter } from "../components/TableOfContents";

const heading = "EX03: Transparency";
const id = "ex03-transparency";

export const Exercise03 = () => {
  return (
    <>
      <TableOfContentsSetter item={{ index: 4, heading, id }} />
      <Article>
        <section>
          <ArticleContent>
            <ArticleHeading id={id}>{heading}</ArticleHeading>
            <p>Coming soon!</p>
          </ArticleContent>
        </section>
      </Article>
    </>
  );
};
