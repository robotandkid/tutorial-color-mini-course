import React, { useMemo } from "react";
import styled from "styled-components";
import {
  Article,
  ArticleContent,
  ArticleHeading,
  ArticleList as ArticleListUnordered,
} from "../components/components";
import { InternalLink, LinkTarget } from "../components/Link";
import { TableOfContentsSetter } from "../components/TableOfContents";
import { hslToRgb, lumaCSS } from "../utils/grayscale";
import { normalizedRandomHue, randomHue, randomNatural } from "../utils/random";
import { DefinitionIds } from "./definitions.types";
import { ColorStrip, Swatch } from "./Swatch";

/**
 * Use this to create a link to a definition.
 *
 * @param props.href - the DefinitionId to link to
 */
export const DefLink = (props: {
  href: DefinitionIds;
  children?: React.ReactNode;
}) => <InternalLink href={`#${props.href}`}>{props.children}</InternalLink>;

const StyledDef = styled.b<{ id?: DefinitionIds }>``;

export const Def = (props: {
  id?: DefinitionIds;
  children?: React.ReactNode;
}) => {
  return !props.id ? (
    <StyledDef>{props.children}</StyledDef>
  ) : (
    <LinkTarget wrapper={StyledDef} id={props.id}>
      {props.children}
    </LinkTarget>
  );
};

const RandomHues = () => {
  const colors = useMemo(
    () => Array.from({ length: 6 }).map(() => `hsl(${randomHue()}, 100%, 50%)`),
    []
  );

  return (
    <ColorStrip>
      {colors.map((color, key) => (
        <Swatch key={key} color={color} />
      ))}
    </ColorStrip>
  );
};

const RandomTintShadeTone = () => {
  const hue = useMemo(() => randomHue(), []);

  const base = `hsl(${hue}, 100%, 50%)`;
  const tint = `hsl(${hue}, 100%, 75%)`;
  const shade = `hsl(${hue}, 100%, 25%)`;
  const tone = `hsl(${hue}, 25%, 50%)`;

  return (
    <ColorStrip>
      <Swatch color={base}>Hue</Swatch>
      <Swatch color={tint}>Tint</Swatch>
      <Swatch color={shade}>Shade</Swatch>
      <Swatch color={tone}>Tone</Swatch>
    </ColorStrip>
  );
};

const RandomToneEquivalent = () => {
  const normalizedHues = useMemo(
    () => Array.from({ length: 6 }).map(normalizedRandomHue),
    []
  );

  const rgb = normalizedHues.map((h) => hslToRgb({ h, s: 1.0, l: 0.5 }));
  const colors = rgb.map(({ r, g, b }) => `rgb(${r}, ${g}, ${b})`);
  const values = rgb.map(lumaCSS);

  return (
    <>
      <ColorStrip>
        {colors.map((hsl, key) => (
          <Swatch key={key} color={hsl} />
        ))}
      </ColorStrip>
      <ColorStrip>
        {values.map((hsl, key) => (
          <Swatch key={key} color={hsl} />
        ))}
      </ColorStrip>
    </>
  );
};

const RandomSaturation = () => {
  const hue = useMemo(() => randomHue(), []);

  const base = `hsl(${hue}, 100%, 50%)`;
  const saturation1 = `hsl(${hue}, 75%, 50%)`;
  const saturation2 = `hsl(${hue}, 50%, 50%)`;
  const saturation3 = `hsl(${hue}, 25%, 50%)`;

  return (
    <ColorStrip>
      <Swatch color={base} />
      <Swatch color={saturation1}></Swatch>
      <Swatch color={saturation2}></Swatch>
      <Swatch color={saturation3}></Swatch>
    </ColorStrip>
  );
};

const randomChromatic = () => {
  const hue = randomHue();
  const saturation = 25 + randomNatural(75);
  const lightness = 25 + randomNatural(75);

  return `hsl(${hue}, ${saturation}%, ${lightness}%)`;
};

const RandomChromatic = () => {
  const colors = useMemo(
    () => Array.from({ length: 6 }).map(randomChromatic),
    []
  );

  return (
    <ColorStrip>
      {colors.map((color, key) => (
        <Swatch key={key} color={color} />
      ))}
    </ColorStrip>
  );
};

const heading = "Basic terms";
const id = "basic-terms";

export const Definitions = () => (
  <>
    <TableOfContentsSetter item={{ index: 1, heading, id }} />
    <Article>
      <section>
        <ArticleContent>
          <ArticleHeading id={id}>{heading}</ArticleHeading>
          <ArticleListUnordered>
            <li>
              <Def id="def-hue">Hue</Def> is the name of the color family. Ex:
              red, yellow, green, etc. A hue is an abstraction, a “pure” color
              without <i>shade</i>,<i>tint</i>, <i>tone</i>, <i>saturation</i>,
              etc.
            </li>
            <li>
              Here are examples of different hues:
              <RandomHues />
            </li>
            <li>
              <Def id="def-color">Color</Def> and <i>hue</i> can be
              interchangeable terms.
            </li>
            <li>
              <Def id="def-tint">Tint</Def> is hue plus white.
            </li>
            <li>
              <Def id="def-shade">Shade</Def> is hue plus black.
            </li>
            <li>
              <Def id="def-tone">Tone</Def> is hue plus gray.
            </li>
            <li>
              Examples of tint, shade, and tone:
              <RandomTintShadeTone />
            </li>
            <li>
              <Def id="def-value">Value</Def> is how light or dark the color is
              with the color removed.
            </li>
            <li>
              That means every color has an equivalent <i>value</i>.
              <RandomToneEquivalent />
            </li>
            <li>
              Another term for value is <i>tone</i>.
            </li>
            <li>
              <Def id="def-saturation">Saturation</Def>, <Def>intensity</Def>,
              and <Def>brilliance</Def> mean the same thing. They refer to
              higher or lower degrees of vividness.
            </li>
            <li>
              Examples of saturation:
              <RandomSaturation />
            </li>
            <li>
              <Def id="def-chromatic">Chromatic</Def> refers to all hues and
              their shades, tines, and tones.
            </li>
            <li>
              Examples of chromatic colors:
              <RandomChromatic />
            </li>
            <li>
              <Def id="def-achromatic">Achromatic</Def> means “no color”, and so
              it refers to all grays and blacks.
            </li>
          </ArticleListUnordered>
        </ArticleContent>
      </section>
    </Article>
  </>
);
