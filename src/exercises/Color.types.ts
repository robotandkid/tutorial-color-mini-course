/**
 * r,g,b - [0, 255]
 */
export interface RGB {
  r: number;
  g: number;
  b: number;
}

export interface Value100 {
  number100: number;
  number255?: undefined;
}

export function Value100(x: number): Value100 {
  return { number100: Math.floor(x) };
}

export interface Value255 {
  number255: number;
  number100?: undefined;
}

export function Value255(x: number): Value255 {
  return { number255: Math.floor(x) };
}

export function diff(a: Value255, b: Value255) {
  return a.number255 - b.number255;
}

export function toValueCSS(value: Value100 | Value255) {
  if (typeof value.number100 !== "undefined") {
    return `hsl(100, 0%, ${value.number100}%)`;
  } else {
    return `hsl(100, 0%, ${Math.floor((value.number255! / 255) * 100)}%)`;
  }
}

export function toRgbCSS(rgb: RGB) {
  return `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`;
}
