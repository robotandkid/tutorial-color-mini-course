import React from "react";
import {
  Article,
  ArticleContent,
  ArticleHeading,
  Blockquote,
} from "../components/components";
import { ExternalLink } from "../components/Link";
import { TableOfContentsSetter } from "../components/TableOfContents";

const Text = () => {
  return (
    <>
      <p>
        This tutorial was inspired by{" "}
        <ExternalLink
          href="https://www.rit.edu/~w-rkelly/html/04_cou/cou_col1.html"
          target="_blank"
        >
          this Mini Course in Color
        </ExternalLink>
        :
      </p>
      <Blockquote>
        “What is described here is not so much a color course as it is a series
        of problems to make students sensitive to color and composition, and to
        further develop eye skills. Sensitivity itself cannot be taught, but
        students can be made aware of it, and they can cultivate their{" "}
        <b>intuitive capabilities.</b>
      </Blockquote>
    </>
  );
};

const heading = "Hello!";
const id = "hello-introduction";

export const Intro = () => {
  return (
    <>
      <TableOfContentsSetter item={{ index: 0, heading, id }} />
      <Article>
        <ArticleHeading id={id}>Hello!</ArticleHeading>
        <section>
          <ArticleContent>
            <Text />
          </ArticleContent>
        </section>
      </Article>
    </>
  );
};
