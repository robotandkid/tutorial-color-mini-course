## Howdy!

This project was inspired by [this mini course in color](https://www.rit.edu/~w-rkelly/html/04_cou/cou_col1.html).

> What is described here is not so much a color course as it is a series of
> problems to make students sensitive to color and composition, and to further
> develop eye skills

That is, the purpose of this tutorial is to help develop "color intuition".

Have fun!

## Running Locally

```bash
yarn dev
```

Runs the app in the development mode.<br/>
Open
[http://localhost:3000](http://localhost:3000) to view it in the browser.

## Style Guide

### Add a New Exercise

Follow this template:

```jsx
<Article> <!-- Semantically, exercises are HTML articles.-->
  <ArticleHeading>Hello!</ArticleHeading>
  <section> <!-- Inside they can have 1 or more HTML sections>
    <ArticleContent>
      Exercise text!
    </ArticleContent>
  </section>
  <section>
    <ArticleContent>
        Another section!
    </ArticleContent>
  </section>
</Article>
```
