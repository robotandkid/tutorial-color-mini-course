import React from "react";
import { GlobalStyle, AppThemeProvider } from "../src/GlobalStyle";

const MyApp = ({
  Component,
  pageProps,
}: {
  Component: any;
  pageProps: any;
}) => {
  return (
    <>
      <GlobalStyle />
      <AppThemeProvider>
        <Component {...pageProps} />
      </AppThemeProvider>
    </>
  );
};

export default MyApp;
