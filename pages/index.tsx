import React from "react";
import App from "../src/App";
import { TableOfContentsItem } from "../src/components/TableOfContents";
import { getStaticProps as staticProps } from "../src/nextjs/getStaticProps";

export default function Index(props: {
  initialTableOfContents?: TableOfContentsItem[];
}) {
  return <App {...props} />;
}

export const getStaticProps = staticProps;
