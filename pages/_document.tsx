import Document, { Html, Head, Main, NextScript } from "next/document";
import { ServerStyleSheet } from "styled-components";

import React from "react";
import { scrollUpButtonId } from "../src/components/ScrollUpButton";

export default class MyDocument extends Document {
  static async getInitialProps(ctx: any) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App: any) => (props: JSX.IntrinsicAttributes) =>
            sheet.collectStyles(<App {...props} />),
        });

      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    return (
      <Html>
        <title>A Mini Color Tutorial</title>
        <Head>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
        </Head>
        <body>
          <Main />
          <div id={`${scrollUpButtonId}`}></div>
          <NextScript />
        </body>
      </Html>
    );
  }
}
